import requests
import json


def get_all_licence_ids():
    spdx_licenses_url = 'https://raw.githubusercontent.com/spdx/license-list-data/master/json/licenses.json'
    r = requests.get(spdx_licenses_url)
    licence_ids_list = []
    for licence in json.loads(r.content)['licenses']:
        licence_ids_list.append(licence['licenseId'])
    return licence_ids_list
