import unittest
from help2copyright.utils import get_all_licence_ids


class TestGetAllLicenceIds(unittest.TestCase):
    def test_get_all_licence_ids(self):
        licenses = get_all_licence_ids()
        self.assertIn("MIT", licenses)
        self.assertIn("NASA-1.3", licenses)
        self.assertIn("Apache-2.0", licenses)
        print(licenses)


if __name__ == '__main__':
    unittest.main()
